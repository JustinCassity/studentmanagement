
public class Student {

	String fName;
	String lName;
	String date;
	int id;
	String fullName;
	
	public Student (String fName, String lName, String date, int id) {
		this.fName = fName;
		this.lName = lName;
		this.date = date;
		this.id = id;
		
	}
	
	public String getFName() {
		return fName;
	}
	
	public String getLName() {
		return lName;
	}
	
	public String getDate() {
		return date;
	}
	
	public int getId() {
		return id;
	}
	
	public String getFullName() {
		fullName = fName + " " + lName;
		return fullName;
	}
}
