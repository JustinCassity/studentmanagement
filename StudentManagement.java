import java.util.ArrayList;

public class StudentManagement{

	public static void main (String[] args) {
		
		ArrayList<Student> students = new ArrayList<Student>();
		
		//create
		Student Justin = new Student ("Justin", "Cassity", "07/31/1989", 101);
		students.add(Justin);
		
		Student Sam = new Student ("Sam", "Green", "01/12/1984", 102);
		students.add(Sam);
		
		Student Chuck = new Student ("Chuck", "Bartowski", "05/18/91", 103);
		students.add(Chuck);
		
		Student Sara = new Student ("Sara", "Smith", "12/20/1994", 104);
		students.add(Sara);
		
		Student Trevor = new Student ("Trevor", "Hambucket", "04/05/1990", 105);
		students.add(Trevor);
		
		Student Alice = new Student ("Alice", "Framstein", "02/27/1980", 106);
		students.add(Alice);
		
		Student Rick = new Student ("Rick", "James", "10/17/1950", 107);
		//read
		System.out.println("Read");
		for (int i = 0; i < students.size(); i++) {
			System.out.println(students.get(i).getFullName() + " " + students.get(i).getDate() + " " + students.get(i).getId());
		}
		//update switches Trevor for Rick
		System.out.println("Update");
		students.set(4,Rick);
		for (int i = 0; i < students.size(); i++) {
			System.out.println(students.get(i).getFullName() + " " + students.get(i).getDate() + " " + students.get(i).getId());
		}
		//delete Deletes Sam
		System.out.println("Delete");
		for (int i = 0; i < students.size(); i++) {
			if (students.get(i).getFullName().contentEquals("Sam Green") && students.get(i).getId() == 102) {
				students.remove(i);
			}
		}
			for (int j = 0; j < students.size(); j++) {
			System.out.println(students.get(j).getFullName() + " " + students.get(j).getDate() + " " + students.get(j).getId());
			
			
		}
			
	}
	
	
	}

